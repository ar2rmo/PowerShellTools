param(
	[parameter(Mandatory=$true)] [string] $DeploymentDomain,
	[parameter(Mandatory=$true)] [string] $DeploymentUser,
	[parameter(Mandatory=$true)] [string] $DeploymentPass,
	
	[parameter(Mandatory=$true)] [string] $ScenarioPath,
	[parameter(Mandatory=$true)] [string] $CiToolsPath,
	[parameter(Mandatory=$true)] [string] $SourcePath,
	
	[parameter(Mandatory=$true)] [string] $SqlServer,
	[parameter(Mandatory=$true)] [string] $Database,
	
	[switch] $SkipImpersonation
)

$__RootPath = Split-Path -parent $PSCommandPath
$__RootPath = Join-Path $__RootPath ".."

."$__RootPath\Tools\BuildScenarioTools.ps1";
."$__RootPath\Tools\ServerTools.ps1";

if (-not $SkipImpersonation){
	Impersonate-User -Domain $DeploymentDomain -Password $DeploymentPass -User $DeploymentUser
}

$ConnectionConfig = New-Object PSObject -Property @{            
			Server = $SqlServer
			Database = $Database
		}

$ScenarioConfig = New-Object PSObject -Property @{            
	        ScenarioPath = $ScenarioPath
			CiToolsPath = $CiToolsPath
			SourcePath = $SourcePath
			ConnectionConfig = $ConnectionConfig
		}

Invoke-Scenario -ScenarioConfig $ScenarioConfig
function Unprotect-File {
	param (
		[string] $SecretsPath,
		[string] $MaskedPath
	)
	
	$h = @{}

	Select-Xml -Path $SecretsPath -XPath "/Secrets/String" | Select-Object -ExpandProperty Node | ForEach-Object { $h.Add($_.Key, $_.Secret) } > $null

	$r = New-Object System.Text.RegularExpressions.Regex("~secret:([A-Za-z][0-9A-Za-z_-]*)~", [System.Text.RegularExpressions.RegexOptions]::Multiline)

	$src = Get-Content $MaskedPath -Raw -Encoding UTF8

	$dst = $r.Replace($src, {
		param ( [System.Text.RegularExpressions.Match] $m )
		$k = $m.Groups[1].Value
		$h.Item($k)
	})

	Write-Output $dst

}

<#
<?xml version="1.0" encoding="utf-8"?>
<Secrets>
	<String Key="Password1" Secret="qweqwe"/>
	<String Key="Password2" Secret="asdasd"/>
</Secrets>
#>

# ~secret:Password1~

# . .\SecuritiesUnmask.ps1; Unprotect-File -SecretsPath Securities.xml -MaskedPath Web.config.masked | Set-Content -Encoding UTF8 -Path Web.config
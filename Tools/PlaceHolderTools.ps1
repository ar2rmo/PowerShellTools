<#
	.SYNOPSIS
		Functions to work with placeholders in strings
	.EXAMPLE	
		Find-PlaceHolders $path | 
		Expand-PlaceHolder $ScenarioConfig | 
		%{ $path = Replace-PlaceHolder $path $_};
#>
function Find-PlaceHolders {
	param (
		[Parameter(Position = 1, Mandatory = $True)]
		[String] $String
	)
	
	process{
		$regex = [regex] "\{[a-zA-Z0-9]+(?:.[a-zA-Z0-9]+)*\}";
		$regex.Matches($String) | select -ExpandProperty Value | Write-Output
	}
}
function Expand-PlaceHolder {
	param (
		[Parameter(Position = 0, Mandatory=$true)]
		[Object] $SourceObject,
		[Parameter(Position = 1, Mandatory=$true, ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[String] $Placeholder
	)	
	process{
		try{
		$objectPath = $Placeholder.Substring(1,$Placeholder.Length-2);
		$objectPathArray = $ObjectPath.Split(".");
		$object = $SourceObject;
		foreach ($propName in $ObjectPathArray){
			$object = $object.$($propName);
		}
		Write-Output @{$($Placeholder) = $object};
		}
		catch {
			Write-Error "Can't convert placeholder $Placeholder to value";
		}
	}
}

function Replace-PlaceHolder {
	param (
		[Parameter(Position = 0, Mandatory=$true)]
		[String] $String,
		[Parameter(Position = 1, Mandatory=$true, ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[hashtable] $ReplaceCollection
	)	
	process{
		foreach ($item in $ReplaceCollection.GetEnumerator()){
			$String = $String.Replace($item.Name, $item.Value);
		}
		Write-Output $String;
	}
}

function Replace-StringPlaceholders {
	param(
		[Parameter(Position = 0, Mandatory=$true)]
		[Object] $SourceObject,
		[Parameter(Position = 1, Mandatory=$true, ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
		[AllowEmptyString()]
		[String] $String
	)
	process {
		if(-not [string]::IsNullOrEmpty($String)){
			Find-PlaceHolders $String | 
			Expand-PlaceHolder $SourceObject | 
			%{ $String = Replace-PlaceHolder $String $_};
		}
		Write-Output $String;
	}
}

$__RootPath = Split-Path -parent $PSCommandPath
$__RootPath = Join-Path $__RootPath ".."

."$__RootPath\Tools\CommonTools.ps1";

function Write-TeamCity-LogRow{
	param(
		[string] $messageType,
		[hashtable] $attributes
	)
	process{
		$stringAttributes = Convert-HashTable-String -Table $attributes -PairDelimiter "=";
		Write-Host "##teamcity[$messageType $stringAttributes]"
	}
}
function Write-TeamCity-BlockOpen{
	param(
		[string] $BlockName
	)
	process{
		$block = new-object PSObject
		$block | add-member -type NoteProperty -Name "Name" -Value $BlockName
		$block | add-Member -memberType ScriptMethod -Name "Close" -Value {
			Write-TeamCity-LogRow -messageType "blockClosed" -attributes @{name = "'"+$this.Name.ToString()+"'"};
		}	
		Write-TeamCity-LogRow -messageType "blockOpened" -attributes @{name = "'"+$BlockName+"'"};
		return $block;
	}
}
function Write-TeamCity-ProgressStart{
	param(
		[string] $BlockName
	)
	process{
		$block = new-object PSObject
		$block | add-member -type NoteProperty -Name "Name" -Value $BlockName
		$block | add-Member -memberType ScriptMethod -Name "Close" -Value {
			Write-TeamCity-LogRow -messageType "progressFinish" -attributes @{name = "'"+$this.Name.ToString()+"'"};
		}	
		Write-TeamCity-LogRow -messageType "progressStart" -attributes @{name = "'"+$BlockName+"'"};
		return $block;
	}
}
function Write-TeamCity-Message{
	param(
		[string] $text,
		[string] $status = "NORMAL",
		[string] $errorDetails = ""
	)
	process{
		Write-TeamCity-LogRow -messageType "message" -attributes @{
			text = "'"+$text+"'";
			status = "'"+$status+"'";
			errorDetails = "'"+$errorDetails+"'";
		};
	}
}

<#
How to Use:
=============================================================
$PackagesBlock = Write-TeamCity-BlockOpen -BlockName "Prepare Packages";
$progressReport = Write-TeamCity-ProgressStart -BlockName "Prepare Packages progress";
Write-TeamCity-Message "Load Data"
Write-TeamCity-Message "Zip packages"
$progressReport.Close();
$PackagesBlock.Close();
=============================================================
#>
$__RootPath = Split-Path -parent $PSCommandPath
$__RootPath = Join-Path $__RootPath ".."

."$__RootPath\Tools\SqlExecuteCommand.ps1";
."$__RootPath\Tools\CommonTools.ps1";
."$__RootPath\Tools\TeamCityTools.ps1";
."$__RootPath\Tools\PlaceHolderTools.ps1";

function Invoke-BuildScenarioInvokePS {
	<#
	.SYNOPSIS
		Executes PS script with arguments
	.PARAMETER $BuildConfig
		Runtime config
	.PARAMETER $ScenarioItem
		Scenario step config
	.EXAMPLE
		{
			"Type": "InvokePS",
			"Params": {
				"FileName": "Test.ps1",
				"Arguments": {
					"Arg1": "Foo",
					"Arg2": "Bar"
				}
			}
		}
	#>
	param(
		[Parameter(Position = 0, Mandatory = $True)]
		[PSObject] $ScenarioConfig,		
		[Parameter(Position = 1, Mandatory = $True)]
		$ScenarioItem
	)
	
	process{
		$block = Write-TeamCity-BlockOpen "PS script: $($ScenarioItem.Params.FileName)";

		$properties = $ScenarioItem.Params.Arguments | Get-Member -MemberType *Property;
		$StepConfig = @{};
		Write-Host "Properties:";
		foreach($property in $properties){
			$propValue = $ScenarioItem.Params.Arguments.$($property.Name)
			$propValue = Replace-StringPlaceholders $ScenarioConfig $propValue;
			Write-Host "	$($property.Name): $propValue";
			$StepConfig.Add($property.Name,$propValue);
		}
		$cmd = Replace-StringPlaceholders $ScenarioConfig $ScenarioItem.Params.FileName;
		if ($cmd -ne $ScenarioItem.Params.FileName){
			Write-Host "Converted FileName [$cmd]"
		}
		&($cmd) $ScenarioConfig $StepConfig

		Write-Host "←[32mInvoking PS script is done."
		
		$block.Close();
	}
}

function Invoke-BuildScenarioInvokeSQL {
	<#
	.SYNOPSIS
		Executes SQL file or query
	.PARAMETER $BuildConfig
		Runtime config
	.PARAMETER $ScenarioItem
		Scenario step config
	.EXAMPLE

		Execute file
			{
				"Type": "InvokeSQL",
				"Params": {
					"FileName": "${UpdateScenarioDirectory}\foo.sql"
				}
			}

		Execute query (NOT IMPLEMENTED)
			{
				"Type": "InvokeSQL",
				"Params": {
					"SQLCommand": "DELETE FROM SysProcessLog"
				}
			}
	#>
	param(
		[Parameter(Position = 0, Mandatory = $True)]
		[PSObject] $ScenarioConfig,		
		[Parameter(Position = 1, Mandatory = $True)]
		$ScenarioItem
	)
	$block = Write-TeamCity-BlockOpen "SQL script: $($ScenarioItem.Params.FileName)";
	
	Write-Host $SqlCommands;
	$FilePath = Replace-StringPlaceholders $ScenarioConfig $ScenarioItem.Params.FileName;
	if ($FilePath -ne $ScenarioItem.Params.FileName){
		Write-Host "Converted FileName [$FilePath]"
	}
	Execute-SQL-Batch -server $ScenarioConfig.ConnectionConfig.Server -dbname $ScenarioConfig.ConnectionConfig.Database -file $FilePath -go

	Write-Host "←[32mInvoking SQL script is done."
	$block.Close();
}

function Get-Scenario {
	<#
	.SYNOPSIS
		Load scenario from jason file
	.PARAMETER $BuildConfig
		Runtime config
	#>
	param(
		[Parameter(Mandatory = $True)]
		[string] $ScenarioPath
	)
	
	process {
	
		Write-Host  "ScenarioPath : $ScenarioPath";
		$ScenarioContent = Get-Content -Raw -Path $ScenarioPath

		$ScenarioContent = $ScenarioContent -ireplace "\\", "\\"
		return $ScenarioContent | Out-String | ConvertFrom-Json
	}
}
function Invoke-Scenario {
	<#
	.SYNOPSIS
		Executes sequence of build steps
		To skip step add "SkipStep": true
	.PARAMETER $ScenarioConfig
		@{            
	        ScenarioPath
			CiToolsPath
			ConnectionConfig @{       
				UserName
				UserDomain
		        UserPassword
				Server
				Database
			}
		}
	#>
	param(
		[Parameter(Mandatory = $True)]
		[PSObject] $ScenarioConfig
	)
	
	process {
		Write-Host "←[34mInvoking application scenario..."

		$Scenario = Get-Scenario -ScenarioPath $ScenarioConfig.ScenarioPath
		$ScenarioItemIndex = 0
		foreach ($ScenarioItem in $Scenario.Steps) {
			Write-Host "←[34mProcessing step $($ScenarioItem.Type) ($($ScenarioItemIndex + 1)/$($Scenario.Steps.Count))"
			$block = Write-TeamCity-ProgressStart "Step $($ScenarioItem.Type) ($($ScenarioItemIndex + 1)/$($Scenario.Steps.Count))";
			
			if ($ScenarioItem.SkipStep -eq $True) {
				Write-Host "Step $($ScenarioItem.Type) ($($ScenarioItemIndex + 1)/$($Scenario.Steps.Count)) skipped."
				$ScenarioItemIndex++;
				continue;
			}
			
			Push-Location
			switch -exact ($ScenarioItem.Type) {
				"InvokeSQL" {
					Invoke-BuildScenarioInvokeSQL $ScenarioConfig $ScenarioItem
				}
				"InvokePS" {
					Invoke-BuildScenarioInvokePS $ScenarioConfig $ScenarioItem
				}
				default {
					Write-Host "Step `'$($ScenarioItem.Type)`' is ignored."
				}
			}
			Pop-Location
			$ScenarioItemIndex++
			$block.Close();
		}

		Write-Host "←[32mInvoking application scenario is done."
	}
}

function Impersonate-User{
	param(
		[string] $Domain,
		[string] $User,
		[string] $Password
	)
	process {
		Write-Host "Impersonating $Domain\$User"
		
		$Global:ImpersonatedUser = @{}
		$tokenHandle = 0
		
		#Import the LogonUser Function from advapi32.dll and the CloseHandle Function from kernel32.dll
		Add-Type -Namespace Import -Name Win32 -MemberDefinition @'
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool LogonUser(string user, string domain, string password, int logonType, int logonProvider, out IntPtr token);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool CloseHandle(IntPtr handle);
'@

		$Global:ImpersonatedUser = @{}
		$tokenHandle = 0

		#Call LogonUser and store its success.  [ref]$tokenHandle is used to store the token "out IntPtr token" from LogonUser.
		$returnValue = [Import.Win32]::LogonUser($User, $Domain, $Password, 2, 0, [ref]$tokenHandle)
		if (!$returnValue) {
		    $errCode = [System.Runtime.InteropServices.Marshal]::GetLastWin32Error();
		    Write-Host "Impersonate-User failed a call to LogonUser with error code: $errCode"
		    throw [System.ComponentModel.Win32Exception]$errCode
		}
		else {
		    #Call the Impersonate method with the returned token. An ImpersonationContext is returned and stored in the
		    #Global variable so that it may be used after script run.
		    $Global:ImpersonatedUser.ImpersonationContext = [System.Security.Principal.WindowsIdentity]::Impersonate($tokenHandle)
		    
		    #Close the handle to the token. Voided to mask the Boolean return value.
		    [void][Import.Win32]::CloseHandle($tokenHandle)

		    Write-Host "Impersonating is done"
		}
	}
}

$WWWPublishingService = 'World Wide Web Publishing Service';
$IISAdminService = 'IIS Admin Service'

function IIS-Stop{
	param(  
		[String] $SiteServer
	) 
	process {
		Write-Host "Load IIS Assembly"
		[reflection.assembly]::loadwithpartialname("System.ServiceProcess")

		Write-Host "Stop IIS"
		(new-Object System.ServiceProcess.ServiceController($WWWPublishingService,"$SiteServer")).Stop() 
		(new-Object System.ServiceProcess.ServiceController($WWWPublishingService,"$SiteServer")).WaitForStatus('Stopped',(new-timespan -seconds 10)) 

		(new-Object System.ServiceProcess.ServiceController($IISAdminService,"$SiteServer")).Stop() 
		(new-Object System.ServiceProcess.ServiceController($IISAdminService,"$SiteServer")).WaitForStatus('Stopped',(new-timespan -seconds 10)) 
		Start-Sleep -s 20
	}
}

function IIS-Start{
	param(  
		[String] $SiteServer
	) 
	process {
		Write-Host "Load IIS Assembly"
		[reflection.assembly]::loadwithpartialname("System.ServiceProcess")

		Write-Host "Start IIS";
		(new-Object System.ServiceProcess.ServiceController($WWWPublishingService,"$SiteServer")).Start() 
		(new-Object System.ServiceProcess.ServiceController($WWWPublishingService,"$SiteServer")).WaitForStatus('Running',(new-timespan -seconds 10)) 

		Start-Sleep -s 20

		(new-Object System.ServiceProcess.ServiceController($IISAdminService,"$SiteServer")).Start() 
		(new-Object System.ServiceProcess.ServiceController($IISAdminService,"$SiteServer")).WaitForStatus('Running',(new-timespan -seconds 10)) 
	}
}

function Service-Stop{
	param(  
		[String] $ServerName,
		[String] $ServiceName
	) 
	process {
		Write-Host "Load Service Process Assembly"
		[reflection.assembly]::loadwithpartialname("System.ServiceProcess")

		Write-Host "Stop $ServiceName on $ServerName";
		(get-service -ComputerName $ServerName -Name $ServiceName).Stop()
		(get-service -ComputerName $ServerName -Name $ServiceName).WaitForStatus('Stopped',(new-timespan -seconds 10)) 
	}
}

function Service-Start{
	param(  
		[String] $ServerName,
		[String] $ServiceName
	) 
	process {
		Write-Host "Load Service Process Assembly"
		[reflection.assembly]::loadwithpartialname("System.ServiceProcess")

		Write-Host "Start $ServiceName on $ServerName";
		(get-service -ComputerName $ServerName -Name $ServiceName).Start()
		(get-service -ComputerName $ServerName -Name $ServiceName).WaitForStatus('Running',(new-timespan -seconds 10)) 
	}
}